require 'test_helper'

class UsersSignupTest < ActionDispatch::IntegrationTest

  test "should not add an invalid user" do
    get signup_path
    assert_no_difference 'User.count' do
      post users_path, user: { name: "", 
                              email: "invalid@email",
                           password: "foobar", 
              password_confirmation: "foobaz" }
              
    end
  end
  
  test "should add a valid user" do
    get signup_path
    name = "Example User"
    email = "user@example.com"
    password = "foobar"
    assert_difference 'User.count', 1 do
      post_via_redirect users_path, user: { name: name,
                                           email: email,
                                        password: password,
                           password_confirmation: password }
    end
    assert_template 'users/show'
    assert is_logged_in?
  end
end
